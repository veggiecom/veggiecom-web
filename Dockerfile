FROM node:12-alpine AS build
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global
ENV PATH=$PATH:/home/node/.npm-global/bin
WORKDIR /home/node
COPY . .
RUN chown -hR node:node /home/node
RUN ls -l /home/node
USER node
RUN npm install -g @angular/cli && \
    npm install && \
    ng build --prod
RUN ls -l /home/node/dist

FROM nginx:1.18-alpine
COPY --from=build /home/node/dist/veggiecom-web /usr/share/nginx/html
